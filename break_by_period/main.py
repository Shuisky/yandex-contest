import sys
from datetime import datetime, timedelta

DAY_FORMAT = "%Y-%m-%d"


def by_week(day):
    SUNDAY = 6
    days_ahead = SUNDAY - day.weekday()
    if days_ahead <= 0:
        days_ahead += 7
    return day + timedelta(days_ahead)


def by_month(day):
    if day.month == 12:
        return day.replace(day=31)
    return day.replace(month=day.month + 1, day=1) - timedelta(1)


def by_quarter(day):
    quarter = (day.month - 1) // 3 + 1
    if quarter == 1:
        return day.replace(month=3, day=31)
    elif quarter == 2:
        return day.replace(month=6, day=30)
    elif quarter == 3:
        return day.replace(month=9, day=30)
    return day.replace(month=12, day=31)


def by_review(day):
    if day.month <= 3 or day.month >= 10:
        return day.replace(month=3, day=31)
    return day.replace(month=9, day=30)


def by_year(day):
    return day.replace(month=12, day=31)


def main():
    INTERVALS = {
        "WEEK": by_week,
        "MONTH": by_month,
        "QUARTER": by_quarter,
        "YEAR": by_year,
        "REVIEW": by_year,
    }
    period = next(sys.stdin).rstrip()
    start_date, end_date = next(sys.stdin).rstrip().split(" ")
    start_date = datetime.strptime(start_date, DAY_FORMAT).date()
    end_date = datetime.strptime(end_date, DAY_FORMAT).date()
    print(break_by_interval(start_date, end_date, INTERVALS[period]))


def break_by_interval(start_date, end_date, next_interval_day):
    result = ""
    while True:
        next_date = next_interval_day(start_date)
        if next_date >= end_date:
            result = (
                result[0:-2]
                + " и с "
                + start_date.strftime(DAY_FORMAT)
                + " по "
                + end_date.strftime(DAY_FORMAT)
            )
            break
        result += (
            "с "
            + start_date.strftime(DAY_FORMAT)
            + " по "
            + next_date.strftime(DAY_FORMAT)
            + ", "
        )

        start_date = next_date + timedelta(1)

    return result


if __name__ == "__main__":
    main()
