# Description
## CODE DOESN'T WORK PROPERLY!!
Hard to describe...
Input is:
  1. first line `num_of_coords scale_of_rectangle`
  2. Second and till end of file is `coords_of_people` of people which ordered some stuff

You have coordinates of people.
You have a rectangle with scale `S`

Task: find the rectangle position with most people
Print in first line `num_of_people`
second line and so on `coords_of_people`
See sample `input.txt` and #Sample usage in this readme for output

# Run
`cat input.txt | python main.py`

# Sample usage
```
❯ cat input.txt | python main.py
3
0 0
2 0
1 1
```
