import sys


def main():
    ns = sys.stdin.readline().rstrip().split(" ")
    # n = int(ns[0]) # not needed because len(array)
    s = int(ns[1])

    coords = []
    for xy_str in sys.stdin:
        x, y = xy_str.rstrip().split(" ")
        coords.append((int(x), int(y)))

    max_x = max_y = 0
    for coord in coords:
        max_x = max(max_x, coord[0])
        max_y = max(max_y, coord[1])

    w = s
    points_count = 0
    for x in range(max_x):
        for y in range(max_y):
            while w:
                res = 0
                bl_corner = (y, y)
                tr_corner = (w, s / w)
                # print(bl_corner, tr_corner)
                for coord in coords:
                    h_line = range(bl_corner[0], tr_corner[0] + 1)
                    v_line = range(bl_corner[1], int(tr_corner[1]) + 1)
                    if coord[0] in h_line and coord[1] in v_line:
                        res += 1
                        points_count = max(points_count, res)
                w -= 1

    print(points_count)


if __name__ == "__main__":
    main()
