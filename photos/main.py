from math import ceil


def main():
    width_to = int(input())
    photos = input().split(" ")
    total_count = int(photos[0])
    count = int(photos[1])
    heights = []
    for _ in range(total_count):
        size_l = input().split("x")
        heights.append(ceil((int(size_l[1]) * width_to) / int(size_l[0])))
    heights.sort()
    max_height = min_height = 0
    for i in range(count):
        max_height += heights[-1 - i]
        min_height += heights[i]
    print(min_height)
    print(max_height)


if __name__ == "__main__":
    main()
