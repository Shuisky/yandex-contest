# Description
Make data structure with complexity O(1)
```
class FastListWithRandom:
    def insert(self, num) -> None:
        pass

    def delete(self, num) -> bool:
        pass

    def find(self, num) -> int:
        pass

    def random(self, num) -> int:
        pass
```
