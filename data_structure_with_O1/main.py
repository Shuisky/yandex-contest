from collections import defaultdict
from random import randint
from typing import Optional


class FastListWithRandom:
    def __init__(self):
        self._lst = []
        self._position = defaultdict(list)

    def insert(self, num) -> None:
        self._lst.append(num)
        self._position[num].append(len(self._lst)-1)

    def delete(self, num) -> bool:
        last_num_dict = self._position[self._lst[-1]]
        if (len(last_num_dict) >= 2 and last_num_dict[-1] < last_num_dict[-2]):
            last_num_dict[-1], last_num_dict[-2] = last_num_dict[-2], last_num_dict[-1]
        if self._position[num] == []:
            return False
        pos = self._position[num][-1]
        self._position[self._lst[-1]][-1] = pos
        self._position[num].pop()
        self._lst[pos], self._lst[-1] = self._lst[-1], self._lst[pos]
        self._lst.pop()
        return True

    def find(self, num) -> Optional[int]:
        if self._position[num] == []:
            return None
        return self._position[num][0]

    def random(self) -> int:
        return self._lst[randint(0, len(self._lst)-1)]

    def print(self) -> None:
        print("List:", "".join([f"{x: >3}" for x in self._lst]))
        print("Indx:", "".join([f"{x: >3}" for x in range(len(self._lst))]))
        print(dict(self._position))


fl = FastListWithRandom()
# Filling with random elements
for _ in range(20):
    fl.insert(randint(0, 9))
# for el in [1, 9, 3, 0, 8, 4, 6, 8, 6, 6]:
#     fl.insert(el)
fl.print()

# Deleting random elements
for _ in range(10):
    r = fl.random()
    print(f"Deleting {r}...", end=" ")
    print(fl.delete(r))
    fl.print()

# for el in [1, 6, 3, 6]:
#     print(f"Deleting {el}...", end=" ")
#     print(fl.delete(el))
#     fl.print()

# Deleting non existand element
r = 999
print(f"Deleting {r}...", fl.delete(r))

# Finding elements
for _ in range(5):
    r = fl.random()
    print(f"Finding {r}...", end=" ")
    print(fl.find(r))

# Finding non existand element
r = 999
print(f"Finding {r}...", fl.find(r))

print("Random element...", fl.random())
