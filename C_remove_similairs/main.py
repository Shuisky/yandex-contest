import sys


def main():
    result = next(sys.stdin).rstrip()
    print(result)
    for num in sys.stdin:
        num = num.rstrip()
        if num != result:
            print(num)
            result = num


if __name__ == "__main__":
    main()
