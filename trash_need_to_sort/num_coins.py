""" counts the number of coins """


class NotEnoughCoinsError(Exception):
    pass


def num_coins_simple(amount: int) -> int:
    """simple solution w/o calculation of coins left"""
    if amount < 1:
        return 0
    coin_count = 0
    for coin in coin_types:
        coin_count += amount // coin
        amount = amount % coin
    return coin_count


def num_coins_by_coins_left(amount: int, coins_left: dict) -> int:
    """extended coins calculation based on coins left"""
    if amount < 1:
        return 0
    coin_count = 0
    for coin in coin_types:
        type_left = coins_left.get(coin, 0)
        coin_type_count = (
            amount // coin if (amount // coin) < type_left else type_left
        )
        coin_count += coin_type_count
        amount -= coin_type_count * coin
        coins_left[coin] -= coin_type_count
    if amount > 0:
        raise NotEnoughCoinsError(f"Not enough coins for {amount}")
    return coin_count


def num_coins(amount=0, coins_left=dict()):
    if not coins_left:
        return num_coins_simple(amount)

    return num_coins_by_coins_left(amount, coins_left)


coins_left = {
    50: 10,
    10: 3,
    5: 20,
    1: 34,
}
coin_types = (50, 10, 5, 1)

print("Coins number:", num_coins(103, coins_left))
print("Coins left:", coins_left)
