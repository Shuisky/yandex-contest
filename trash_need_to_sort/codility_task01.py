"""
You are the new sheriff in town! You are here to maintain law and order! You can't do this without clean and validated data. This task is about Django views that you will use to keep your town safe.
Write a Django view to validate basic input data. The view should only respond to http methods of type "GET" or "POST", and all other http methods should be rejected. For the "POST" http method, the view should receive the following data parameters (all of which are of type string, and none of which is required): [first_name, last_name, phone, email].
Requirements:
1. The view name should be "test_view"; please be sure not to change it, as it will be imported with this name.
2. If the view receives a "GET" request it should return an http response with the message, "This is a get request".
3. If the view receives a "POST" request it should validate the four attributes being posted.
4. The first attribute is "first_name" and it should contain only characters and numbers (no special characters). If it doesn't, the response should include the message, "First name should only contain characters and numbers without any spaces or special characters".
5. The second attribute is "last_name" and it should contain only characters and numbers (no special characters). If it doesn't, the response should include the message, "Last name should only contain characters and numbers without any spaces or special characters".
6. The third attribute is "phone" and it should contain only numbers and have a maximum length of 10 digits. If it doesn't, the response should include the message, "Phone number should only contain numbers and should have maximum length of 10 digits".
7. The fourth attribute is "email", which should have the format: "example@domain.com". The email name can contain special characters like [~, !, #, $, %, ^, &amp;, *, _], but may not contain [@, (, ), .]. The email domain and subdomain should not contain special characters. All three parts of the email are required. If any of these rules is violated, the response should include the message, "Invalid email. Email should have the format: example@domain.com".
Examples of valid emails are:
mail@domain.com
mail_*&amp;^%$#!~?@domain.com

Examples of invalid emails are:
mail.@somewhere.com because email name contains .
something()@somewhere.com  because email name contains ()
something@somewhere_?.com  because email subdomain contains _?
something@somewhere.com!   because email domain contains !

8. If multiple validation errors occur, the response should include all appropriate validation error messages. For example: by posting "!@#" as first_name and "%^&amp;" as last_name you should see two lines of error responses, each of them containing the validation message for the appropriate parameter; for instance:
"First name should only contain characters and numbers without any spaces or special characters"
"Last name should only contain characters and numbers without any spaces or special characters"
9. Empty or missing input is valid for all attributes.
10. If all parameters are correct (or empty) the response should be plain text containing the string: "Your data is valid".
11. For any http method other than "GET" and "POST", the response code should be 405 Method not allowed.
12. The response format should be "text/plain".
Assessment/Tools

Django version is 2.0.5.
You can import Django and all standard Python libraries.
Performance is not assessed; focus only on the correctness of the tests.

Copyright 2009&ndash;2020 by Codility Limited. All Rights Reserved. Unauthorized copying, publication or disclosure prohibited.
"""
