print(not 0)  # True
print(not 2)  # False

print(0 and 3)  # 0
print(2 and 3)  # 3

print(0 or 3)  # 3
print(2 or 3)  # 2

print(5 / 2)  # 2.5


x = [[]] * 3
x[0].append("a")
x[1].append("b")
x[2].append("c")
x[0] = ["d"]
print(x)  # [['d'], ['a', 'b', 'c'], ['a', 'b', 'c']]
print("Id x:", id(x))
for v in x:
    print("Id of ", v, "element: ", id(v))
