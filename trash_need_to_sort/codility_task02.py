"""
You are given two Django models, Author and Book. A book has an author, which is a reference to an Author, and a copies_sold field, which indicates how many copies of that book have been sold in the bookstore.
We want to find out the number of copies sold per author, which is the sum of copies sold of all books that the author has written.
Write a method annotate_with_copies_sold that will annotate authors with a copies_sold attribute. The method should be available on the Author's default model manager (Author.objects).
The method should also be chainable from any Author queryset. That means, any queryset you get from Author.objects should also have the method (e.g. Author.objects.filter(..).exclude(..).annotate_with_copies_sold()).
The return type of the method should be a queryset.
For example, assuming the following data:
author = Author.objects.create(first_name='Mark', last_name='Twain')
Book.objects.create(author=author, title='Adventures of Huckleberry Finn', copies_sold=7)
Book.objects.create(author=author, title='The Adventures of Tom Sawyer', copies_sold=4)
the code
author = Author.objects.annotate_with_copies_sold().first()
should return an Author object with an additional field copies_sold:
" author.copies_sold
11
Assessment/Tools

Django version is 2.0.5.
The solution shouldn't generate unnecessary SQL queries.
You can import Django and all standard Python libraries.
Copyright 2009-2020 by Codility Limited. All Rights Reserved. Unauthorized copying, publication or disclosure prohibited.
"""
