print("Task 1: make list of given elements less than 5")


def less_than_5(numbers_list: list):
    """returns list of elements less than 5"""
    return [x for x in numbers_list if x < 5]


print("1 task:", less_than_5([1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]))

print("===================")
print("Task 2: unite two given lists")


def shows_common_items1(first_list: list, second_list: list):
    """colhoz way"""
    result = []
    switch = len(first_list) > len(second_list)
    cycle_list = second_list if switch else first_list
    for num in cycle_list:
        if num in [first_list, second_list][not switch]:
            result.append(num)
    return result


a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
b = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
print("2 task Colhoz way:", shows_common_items1(a, b))
print("2 task filter way:", list(filter(lambda i: i in a, b)))
print("2 task generator way:", [item for item in a if item in b])
print("2 task set way:", list(set(a) & set(b)))

print("===================")
print("Task 3: sort dict by value asc, desc")


def sort_dict_by_value(dictionary: dict, reverse=False):
    """there is a way to do it with 'operator' module
    like this:
    result = dict(sorted(dictionary.items(), key=operator.itemgetter(1), reverse=reverse))
    """
    result = dict()
    temp_dict = dict()
    for k, v in dictionary.items():
        temp_dict[v] = k
    temp_list = sorted(temp_dict, reverse=reverse)
    for i in temp_list:
        result[temp_dict[i]] = i

    # import operator
    # return dict(sorted(dictionary.items(), key=operator.itemgetter(1), reverse=reverse))
    return result


dict_ex = {0: 2, 1: 1, "sdf": 3, "name": 6}
print("3 task ascending order: ", sort_dict_by_value(dict_ex))
print("3 task descending order: ", sort_dict_by_value(dict_ex, reverse=True))

print("===================")
print("Task 4: merge several dictionaries")

dict_a = {1: 1, 2: 2, 3: 3}
dict_b = {3: 3, 4: 4, 5: 5}
dict_c = {5: 5, 6: 6, 7: 7}

print("4 task: ", {**dict_a, **dict_b, **dict_c})

print(int("ABddCD", 16))
