def gcd(a, b):
    if not a:
        return b
    if not b:
        return a
    if a == b:
        return a
    if a > b:
        return gcd(a % b, b)
    if b < a:
        return gcd(a, b % a)


def main():
    a, b = 18, 35
    print(gcd(a, b))


if __name__ == "__main__":
    main()
