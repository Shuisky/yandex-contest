""" calculates fibbonacci number with cache and count calls """
import time


def count_calls(func):
    def wrapper(*args, **kwargs):
        wrapper.fib_count_calls += 1
        return func(*args, **kwargs)

    wrapper.fib_count_calls = 0
    return wrapper


def cache(func):
    def wrapper(*args, **kwargs):
        cache_key = args + tuple(kwargs.items())
        if cache_key not in wrapper.cache:
            wrapper.cache[cache_key] = func(*args, **kwargs)
        return wrapper.cache[cache_key]

    wrapper.cache = dict()
    return wrapper


@count_calls
@cache
def _real_fib(num):
    if num < 2:
        return 1
    return _real_fib(num - 1) + _real_fib(num - 2)


def fib_wo_cache(num):
    return _real_fib(num)


def fib_w_cache(num):
    return _real_fib(num)


number = 100

start_time = time.time()
# print(f"Fibbonacci of {number}:", fib_wo_cache(number))
execution_time = time.time() - start_time
# print("Calls of fib func w cache:", _real_fib.fib_count_calls)
# print("Time w cache:", execution_time)

for i in range(10):
    print(bin(i), bin(i)[3:])
