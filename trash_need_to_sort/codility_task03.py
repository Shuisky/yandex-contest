"""
<p>You are writing a simple CRUD application in Django (2.0.5) for managing a zoo. It has to allow
particular animals to be added and removed, and to track the time when they were last fed.
Animals must have names that are unique across all species. Unfortunately, the creator of this
model didn't provide unique constraints regarding the name at the database level, so you need
to take this into account in the views.</p>
<p>Models are implemented in the following way:</p>
<pre><code>from django.utils import timezone
from django.db import models

class Species(models.Model):
    name = models.CharField(max_length=30, primary_key=True)

class Animal(models.Model):
    name = models.CharField(max_length=30)
    species = models.ForeignKey(Species, on_delete=models.CASCADE)
    last_feed_time = models.DateTimeField(default=timezone.now)
</code></pre>
<h4>Requirements</h4>
<ol>
<li>
<p>Implement a <code>GET</code> request handler in the class-based view <code>AnimalPopulationView</code> that returns the total number of animals in the zoo.</p>
<ul>
<li>It should return an HttpResponse with a number.</li>
</ul>
</li>
<li>
<p>Implement a <code>GET</code> request handler in the class-based view <code>AnimalView</code> that returns data about an animal in JSON format. <br/></p>
<ul>
<li>It should return data in the following format: <code>{&quot;name&quot;: &quot;name&quot;, &quot;species&quot;: &quot;species&quot;, &quot;last_feed_time&quot;: &quot;last_feed_time&quot;}</code>.</li>
<li>You should query animals using their name, which is passed as <code>GET</code> parameter <code>name</code>.</li>
<li>If there is no animal of the specified name, the view should return a response with a 404 status.</li>
</ul>
</li>
<li>
<p>Implement a <code>POST</code> request handler in the class-based view <code>AnimalView</code> that creates an animal.</p>
<ul>
<li>Perform the unique name validation there. Return the data relating to the created animal in the same format as in requirement 2.</li>
<li>Perform the name and species length validation there (for example, the model's maximum name length should be 30 characters).</li>
<li>If the animal was added successfully, return a status code of 201; or 422 otherwise.</li>
<li>If the animal &quot;species&quot; you are creating doesn't exist, it should be created.</li>
<li>The request will include the parameters [&quot;name&quot;, &quot;species&quot;] passed as <code>POST</code> parameters.</li>
</ul>
</li>
<li>
<p>Implement a <code>GET</code> request handler in the class-based view <code>HungryAnimalsView</code> that returns animals that have not been fed for at least two days.</p>
<ul>
<li>It should return an HttpResponse with a number.</li>
</ul>
</li>
<li>
<p>Implement a <code>POST</code> request handler in the class-based view <code>FeedAnimalView</code> that updates an animal's <code>last_feed_time</code> to the current time.</p>
<ul>
<li>To set the current time, use <code>timezone</code> from <code>django.utils</code>.</li>
<li>The request will include the parameter [&quot;name&quot;] passed as <code>POST</code> parameters.</li>
</ul>
</li>
</ol>
<h4>Assessment/Tools</h4>
<ul>
<li>Django version is 2.0.5.</li>
<li>You can import Django and all standard Python libraries.</li>
<li>Performance is not assessed; focus only on the correctness of the tests.</li>
<li>Don't change the names of the views.</li>
</ul>
"""
