""" simple generator """


def gen_numbers(num):
    while num > 0:
        yield num
        num -= 1


for i in gen_numbers(6):
    print(i)
