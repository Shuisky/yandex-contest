import sys


def main():
    binary_vector = next(sys.stdin).strip()
    result = count = 0
    for n in binary_vector:
        if n == "1":
            count += 1
        else:
            result = max(result, count)
            count = 0
    print(max(result, count))


if __name__ == "__main__":
    main()
