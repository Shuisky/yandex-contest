# Yandex contest

## Description
It is collection of tasks from Yandex contest
Mainly they check algorithmic skills
My solutions *can have some bugs*

## Test cpu and time
install `time` package, usually SHELLs using their own realization
run.sh content is like `cat input | python main.py`
Export new format for `time` like that `export TIME='\t%E real,\t%U user,\t%S sys,\t%K amem,\t%M mmem'`
After that you need to run `/usr/bin/time sh run.sh`

## License
BSD
