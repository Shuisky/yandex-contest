import sys


def main():
    # I use slice instead of strip, because `space` symbol could be jewel
    j = next(sys.stdin)[:-1]
    s = next(sys.stdin)[:-1]
    num = 0
    for i in s:
        if i in j:
            num += 1
    print(num)


if __name__ == "__main__":
    main()
