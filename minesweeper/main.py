import sys


def main():
    n, m = next(sys.stdin).split(" ")
    n = int(n)
    m = int(m)
    next(sys.stdin)  # don't need nums of mines
    coords = []
    for i in sys.stdin:
        r, c = i.split(" ")
        coords.append((int(r), int(c)))

    field = build_field(n, m, coords)
    print_field(field)
    print(clicks(field))


def build_field(n, m, coords):
    field = [[0 for _ in range(n)] for _ in range(m)]
    for r, c in coords:
        field[c - 1][r - 1] = 1
    return field


def clicks(field):
    count = 0
    for i in range(len(field)):
        for j in range(len(field[0])):
            if field[i][j] == 0:
                reveal(i, j, field)
                count += 1
    return count


def reveal(i, j, field):
    for_reveal = set()
    for_reveal.add((i, j))
    while for_reveal != set():
        (x, y) = for_reveal.pop()
        if field[x][y] != 0:
            continue
        field[x][y] = 2
        if x > 0:
            for_reveal.add((x - 1, y))
        if y > 0:
            for_reveal.add((x, y - 1))
        if x < len(field) - 1:
            for_reveal.add((x + 1, y))
        if y < len(field[0]) - 1:
            for_reveal.add((x, y + 1))


def recursion_reveal(i, j, field):
    """This function is ok, but have some problems with recursion limit"""
    field[i][j] = 2
    if i > 0 and field[i - 1][j] == 0:
        reveal(i - 1, j, field)
    if j > 0 and field[i][j - 1] == 0:
        reveal(i, j - 1, field)
    if i < len(field) - 1 and field[i + 1][j] == 0:
        reveal(i + 1, j, field)
    if j < len(field[0]) - 1 and field[i][j + 1] == 0:
        reveal(i, j + 1, field)


def print_field(field):
    CRED = "\033[91m"
    CGREEN = "\033[92m"
    CBLUE = "\033[94m"
    CEND = "\033[0m"
    print(
        CBLUE + "█" * (len(field[0]) + 2) + CEND,
        sep="",
    )
    for i in range(len(field)):
        print(CBLUE + "█" + CEND, end="")
        for j in range(len(field[0])):
            if field[i][j] == 1:
                print(CRED + "█" + CEND, end="")
            elif field[i][j] == 2:
                print(CGREEN + "█" + CEND, end="")
            else:
                print(" ", end="")
        print(CBLUE + "█" + CEND)
    print(
        CBLUE + "█" * (len(field[0]) + 2) + CEND,
        sep="",
    )


if __name__ == "__main__":
    main()
