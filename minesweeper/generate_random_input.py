import random


def main():
    """Generating random input.txt
    Use > to output into some file"""
    n = random.randint(1, 100)
    m = random.randint(1, 100)
    print(n, m)
    k = random.randint(1, n * m)
    print(k)
    for _ in range(k):
        print(random.randint(1, n), random.randint(1, m))


if __name__ == "__main__":
    main()
