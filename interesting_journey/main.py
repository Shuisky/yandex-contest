"""Time limit exceeded... need more optimization"""
import sys


def abs(value: int) -> int:
    return value if value > 0 else -value


def main():
    cities_num = int(sys.stdin.readline().rstrip())
    cities_coords = []
    for _ in range(cities_num):
        x, y = sys.stdin.readline().rstrip().split(" ")
        cities_coords.append((int(x), int(y)))
    fuel = int(sys.stdin.readline().rstrip())
    direction = sys.stdin.readline().rstrip().split(" ")
    start_city = int(direction[0])
    end_city = int(direction[1])

    # print(f'Cities num: {cities_num}')
    # print(f'Cities coords: {cities_coords}')
    # print(f'Fuel: {fuel}')
    # print(f'Start city: {start_city}')
    # print(f'End city: {end_city}')

    end = cities_coords[end_city - 1]
    start = cities_coords.pop(start_city - 1)
    result = find_way(start, cities_coords, end, fuel)
    print(result if result else -1)


def find_way(from_city, cities_coords, to_city, fuel):
    jumps = 0
    # print(from_city, cities_coords, to_city, fuel)
    for i, city in enumerate(cities_coords):
        distance = abs(city[0] - from_city[0]) + abs(city[1] - from_city[1])
        if distance <= fuel and to_city == city:
            return 1
        elif distance <= fuel:
            cc = cities_coords[:]
            c = cc.pop(i)
            jumps += find_way(c, cc, to_city, fuel)

    return jumps


if __name__ == "__main__":
    main()
