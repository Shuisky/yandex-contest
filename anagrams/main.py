import sys


def main():
    collection = [{}, {}]
    i = 0
    for x in sys.stdin.read():
        if x == "\n":
            i = 1
            continue
        if x in collection[i]:
            collection[i][x] += 1
        else:
            collection[i][x] = 1
    print(1 if collection[0] == collection[1] else 0)


if __name__ == "__main__":
    main()
