# Description
## Use Google translate, it is original text
Даны две строки, состоящие из строчных латинских букв. Требуется определить, являются ли эти строки анаграммами, т. е. отличаются ли они только порядком следования символов.

# Run
`cat input.txt | python main.py`

# Sample usage
```
❯ cat input.txt
asdfghjkl
nvienvksndfinevlndpvinjlnpvijnwefnvinpivnwekdnfvfsdnfvlkenfvindskfjnvonefvksnpdvnwoevosnvosevrn
```
```
❯ cat input.txt | python main.py
0
```
