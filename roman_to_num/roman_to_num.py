class Solution:
    def romanToInt(self, s: str) -> int:
        if not s:
            return 0
        result = midr = 0

        hm = {
            "I": 1,
            "V": 5,
            "X": 10,
            "L": 50,
            "C": 100,
            "D": 500,
            "M": 1000,
        }
        midr = hm[s[0]]
        for i in range(1, len(s)):
            midr += hm[s[i]]

            if hm[s[i]] > hm[s[i - 1]]:
                result += midr
                midr = 0
        result += midr
        return result


sol = Solution()
print("MCMXCIV", sol.romanToInt("MCMXCIV"), sol.romanToInt("MCMXCIV") == 1994)
print("III", sol.romanToInt("III"), sol.romanToInt("III") == 3)
print("LVIII", sol.romanToInt("LVIII"), sol.romanToInt("LVIII") == 58)
