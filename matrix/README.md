# Description

Real world yandex contest problem
I didn't copied exact descriptioon, so I try to write in my own words.

Input is just a number.

1. You must make a matrix with width and height equals 2*number+1. Like checkerboard with "black" and "white" imaginary cells.
   For example: Input 2. Size equals 5 (2*2+1)
2. First diagonal numbers should be only zeroes.
3. All white cells should be increasing numbers by 1 with first number equaled also 1. Like 1 2 3 4, but only "white" cells
4. All other "black" cells should be decreasing by 1 of -1. Ex. -1 -2 -3, but in vertical order.

# Run

`echo 2 | python main.py`

# Sample usage

```
❯ echo 2 | python main.py
0 1 -4 2 -7
3 0 4 -6 5
-1 6 0 7 -8
8 -3 9 0 10
-2 11 -5 12 0
```
