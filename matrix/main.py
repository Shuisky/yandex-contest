def main():
    size = 2 * int(input()) + 1
    matrix = [["0" for _ in range(size)] for _ in range(size)]
    positive = negative = 1
    for i in range(size):
        for j in range(size):
            if i == j:
                continue
            if (i + j) % 2 == 1:
                matrix[i][j] = str(positive)
                positive += 1
            else:
                matrix[j][i] = str(-negative)
                negative += 1

    for row in matrix:
        print(" ".join(row))


if __name__ == "__main__":
    main()
