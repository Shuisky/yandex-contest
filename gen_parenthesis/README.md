# Description
Generate all parenthesis combinations
**CAUTION!! For num greater than 15 CPU will blow up!^^**

# Run
`python main.py`

# Sample usage
```
❯ python main.py
(((())))
((()()))
((())())
((()))()
(()(()))
(()()())
(()())()
(())(())
(())()()
()((()))
()(()())
()(())()
()()(())
()()()()
```
