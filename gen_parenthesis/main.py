def make_parenthesis(cur, open, closed, n):
    if len(cur) == 2 * n:
        print(cur)
        return
    if open < n:
        make_parenthesis(cur + "(", open + 1, closed, n)
    if closed < open:
        make_parenthesis(cur + ")", open, closed + 1, n)


def main():
    make_parenthesis("", 0, 0, 4)


if __name__ == "__main__":
    main()
